<?php

namespace Drupal\ds_switch_view_mode_condition\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Condition plugin for the Display Suite Switch View Mode field.
 *
 * @Condition(
 *   id = "ds_switch",
 *   label = @Translation("Display Suite switch view mode"),
 *   context = {
 *     "node" = @ContextDefinition("entity:node",
 *       label = @Translation("Node")
 *     )
 *   }
 * )
 */
class DsSwitchCondition extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Default view mode value.
   */
  const DEFAULT_VIEW_MODE = 'default';

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Creates a new DsSwitchCondition instance.
   *
   * @param array $configuration
   *   The condition configuration.
   * @param string $plugin_id
   *   The plugin_id of the condition.
   * @param mixed $plugin_definition
   *   The plugin definition implementation.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              EntityDisplayRepositoryInterface $entity_display_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'view_modes' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['view_modes'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t("View modes"),
      '#options' => $this->getViewModeOptions(),
      '#default_value' => $this->configuration['view_modes'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['view_modes'] = array_values(array_filter($form_state->getValue('view_modes')));
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    if (empty($this->configuration['view_modes'])) {
      return $this->t('Not restricted');
    }

    $negated = $this->isNegated();
    $view_modes = $this->configuration['view_modes'];

    // Get list of selected view mode labels.
    $view_mode_labels = array_intersect_key($this->getViewModeOptions(), array_flip($view_modes));

    return $this->t($negated ? 'View mode is not one of: %view_modes' : 'View mode is one of: %view_modes', [
      '%view_modes' => implode(', ', $view_mode_labels),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    if (empty($this->configuration['view_modes'])) {
      return TRUE;
    }

    $view_modes = $this->configuration['view_modes'];

    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->getContextValue('node');

    // Handle default view mode case correctly.
    // If the entity does not have the ds_switch field default view mode is
    // assumed.
    $view_mode = $node->hasField('ds_switch') ? $node->ds_switch->value : NULL;
    if (empty($view_mode)) {
      $view_mode = static::DEFAULT_VIEW_MODE;
    }

    // Check view mode.
    if (!in_array($view_mode, $view_modes)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Returns an array of available view mode options.
   *
   * @return array
   *   Array of available view mode labels keyed by id.
   */
  protected function getViewModeOptions() {
    $options = [static::DEFAULT_VIEW_MODE => $this->t("Default")];
    $options += $this->entityDisplayRepository->getViewModeOptions('node');
    return $options;
  }

}
